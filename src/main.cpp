#include "WProgram.h"
#include "src/Bounce2.h" // TODO: Unused.
#include "registers.h"

#define FPGA Serial1

const uint8_t RX = 0;
const uint8_t TX = 1;
const uint8_t LED = 13;
const uint8_t POT2 = 31;
const uint8_t POT1 = 32;

/////////////////////////////////////////////////
// FPGA                                        //
/////////////////////////////////////////////////

// TODO: Better logging.
// TODO: Option to wait for a response/timeout.
uint8_t fpga_recv(uint32_t timeout=10)
{
    elapsedMillis waiting;
    uint8_t value;

    while ((waiting < timeout) && (FPGA.available() < 1)) {}
    value = FPGA.read();

    char str[100];
    snprintf(str, sizeof(str), "recv: %02X", value);
    Serial.println(str);

    return value;
}

uint8_t fpga_send(uint8_t byte)
{
    FPGA.write(byte);
    char str[100];
    snprintf(str, sizeof(str), "send: %02X", byte);
    Serial.println(str);
    // TODO: Does this actually work?
    return fpga_recv();
}

// TODO: Use recv data to stay aligned.
void fpga_send_pkt(uint16_t addr, uint8_t value)
{
    // The message state should be 0.
    while(fpga_send(addr >> 8) != 0) {}; // MSG_ADDR_MSB
    fpga_send(addr & 0x00FF);            // MSG_ADDR_LSB
    fpga_send(value);                    // MSG_VALUE
    fpga_send(0x00);                     // MSG_CRC
}

/////////////////////////////////////////////////
// Channels                                    //
/////////////////////////////////////////////////

// TODO: Handle midi note off for all channels.
// TODO: Use midi control change for other parameters.
// TODO: Use velocity for initial volume envelope.
// TODO: Does this even need to be a class really?
// TODO: This is very repetitive code... rethink.
class Pulse_1 {
  public:
    uint8_t nr10 = 0x00;
    uint8_t nr11 = 0x40;
    uint8_t nr12 = 0xFF;
    uint8_t nr13 = 0x00;
    uint8_t nr14 = 0x00;

    void midi_note_on(uint16_t note, uint8_t velocity) {
        nr11 = (uint8_t)floor(((float)analogRead(POT1) / 0x3FF) * 0x04) << 6;
        nr12 = floor(((float)analogRead(POT2) / 0x3FF) * 0xFF);
        nr13 = note & 0xFF;
        nr14 = (nr14 >> 3 << 3) | (note >> 8);

        fpga_send_pkt(NR10, nr10);
        fpga_send_pkt(NR11, nr11);
        fpga_send_pkt(NR12, nr12);
        fpga_send_pkt(NR13, nr13);
        fpga_send_pkt(NR14, nr14);
        delay(1);
        fpga_send_pkt(NR14, nr14 | 0x80);
    }

} pulse_1;


class Pulse_2 {
  public:
    uint8_t nr21 = 0x40;
    uint8_t nr22 = 0xFF;
    uint8_t nr23 = 0x00;
    uint8_t nr24 = 0x00;

    void midi_note_on(uint16_t note, uint8_t velocity) {
        nr21 = (uint8_t)floor(((float)analogRead(POT1) / 0x3FF) * 0x04) << 6;
        nr22 = floor(((float)analogRead(POT2) / 0x3FF) * 0xFF);
        nr23 = note & 0xFF;
        nr24 = (nr24 >> 3 << 3) | (note >> 8);

        fpga_send_pkt(NR21, nr21);
        fpga_send_pkt(NR22, nr22);
        fpga_send_pkt(NR23, nr23);
        fpga_send_pkt(NR24, nr24);
        delay(1);
        fpga_send_pkt(NR24, nr24 | 0x80);
    }

} pulse_2;

class Wave {
  public:
    uint8_t nr30 = 0x80;
    uint8_t nr31 = 0x00;
    uint8_t nr32 = 0x20;
    uint8_t nr33 = 0x00;
    uint8_t nr34 = 0x00;

    void midi_note_on(uint16_t note, uint8_t velocity) {
        nr33 = note & 0xFF;
        nr34 = (nr34 >> 3 << 3) | (note >> 8);

        fpga_send_pkt(NR30, nr30);
        fpga_send_pkt(NR31, nr31);
        fpga_send_pkt(NR32, nr32);
        fpga_send_pkt(NR33, nr33);
        fpga_send_pkt(NR34, nr34);
        delay(1);
        fpga_send_pkt(NR34, nr34 | 0x80);
    }

    void midi_note_off() {
        fpga_send_pkt(NR30, 0x00);
        fpga_send_pkt(NR34, nr34);
        delay(1);
        fpga_send_pkt(NR34, nr34 | 0x80);
    }

} wave;

class Noise {
  public:
    uint8_t nr41 = 0x00;
    uint8_t nr42 = 0x00;
    uint8_t nr43 = 0x00;
    uint8_t nr44 = 0x00;

    void midi_note_on(uint8_t note, uint8_t velocity) {
        nr42 = floor(((float)analogRead(POT2) / 0x3FF) * 0xFF);
        nr43 = note & 0xFF;

        fpga_send_pkt(NR41, nr41);
        fpga_send_pkt(NR42, nr42);
        fpga_send_pkt(NR43, nr43);
        fpga_send_pkt(NR44, nr44);
        delay(1);
        fpga_send_pkt(NR44, nr44 | 0x80);
    }

} noise;

/////////////////////////////////////////////////
// MIDI                                        //
/////////////////////////////////////////////////

// TODO: This is different for the wave channel.
uint16_t to_gb_note(uint8_t midi_note)
{
    return 2048 - (131072 / (pow(2, (float)(midi_note - 60) / 12) * 440));
}

void midi_note_on(byte channel, byte note, byte velocity)
{
    Serial.println("midi_note_on");
    switch(channel) {
        case 1:
            pulse_1.midi_note_on(to_gb_note(note), velocity);
            break;
        case 2:
            pulse_2.midi_note_on(to_gb_note(note), velocity);
            break;
        case 3:
            wave.midi_note_on(to_gb_note(note), velocity);
            break;
        case 4:
            noise.midi_note_on(note, velocity);
            break;
    }
}

void midi_note_off(byte channel, byte note, byte velocity)
{
    switch(channel) {
        case 3:
            wave.midi_note_off();
            break;
    }
}

/////////////////////////////////////////////////
// Main                                        //
/////////////////////////////////////////////////

void loop()
{
    usbMIDI.read();
}

void setup()
{
    Serial.begin(115200);
    FPGA.begin(115200);

    pinMode(LED, OUTPUT);
    digitalWrite(LED, HIGH);

    pinMode(POT1, INPUT);
    pinMode(POT2, INPUT);

    usbMIDI.read();
    usbMIDI.setHandleNoteOn(midi_note_on);
    usbMIDI.setHandleNoteOff(midi_note_off);
}

// TODO: Clean up todos (lol).
extern "C" int main(void)
{
    setup();
    while (1) {
        loop();
    }
}
