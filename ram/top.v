module top
(
    // 16 address pins
    input  [15:0] address,

    // Data pins (bidirectional)
    inout [7:0] data,

    // Signals from cartridge
    input nWR,   // Write
    input nRD,   // Read
    input nCS,   // Chip-select

    // Output enable for data level shifter
    output OE,

    // 16 MHz clock from FPGA
    input clk_16mhz,

    // UART interface
    input uart_rx,
    output uart_tx
);

// Disable USB [https://discourse.tinyfpga.com/t/why-drive-usb-pull-up-resistor-to-0-to-disable-usb/1276/2]
assign pin_pu = 0;

reg [7:0] data_out = 8'd0;
reg output_enable = 1'd0;

// Enable data output if required, otherwise high-impedance
assign data = output_enable ? data_out : 8'bzzzzzzzz;
assign OE = output_enable;

always @(posedge clk_100mhz) begin
    // TODO:
    //     if (nRD || ~nWR || ~nCS) begin
    //         output_enable <= 1'd0;
    //     end
    if (address >= 16'hA000 && address <= 16'hA03F) begin
        data_out <= ram[address - 16'hA000];
        output_enable <= 1'd1;
    end else begin
        output_enable <= 1'd0;
    end
end

// UART
wire [7:0] uart_rx_byte;
reg [7:0] uart_tx_byte;
wire uart_received;
reg uart_transmit = 1'b0;

uart #(
    .baud_rate(115200),
    .sys_clk_freq(100000000)
) uart0 (
    .clk(clk_100mhz),
    // .rst(),
    .rx(uart_rx),
    .tx(uart_tx),
    .transmit(uart_transmit),
    .tx_byte(uart_tx_byte),
    .received(uart_received),
    .rx_byte(uart_rx_byte),
    // .is_receiving(),
    // .is_transmitting(),
    .recv_error()
);

reg [7:0] ram [0:64];

localparam [1:0]
    MSG_ADDR_MSB = 2'd0,
    MSG_ADDR_LSB = 2'd1,
    MSG_VALUE    = 2'd2,
    MSG_CRC      = 2'd3;
reg [1:0] msg_state = MSG_ADDR_MSB;
reg [15:0] msg_address = 0;
reg [7:0] msg_value = 0;

always @(posedge clk_100mhz) begin
    if (uart_received) begin
        // Echo the message state.
        uart_tx_byte <= msg_state;
        uart_transmit <= 1'b1;
        // Handle message state.
        case (msg_state)
            MSG_ADDR_MSB: begin
                msg_address <= (msg_address | uart_rx_byte) << 8;
            end
            MSG_ADDR_LSB: begin
                msg_address <= msg_address | uart_rx_byte;
            end
            MSG_VALUE: begin
                msg_value <= uart_rx_byte;
            end
            MSG_CRC: begin
                // TODO: Actually check a CRC.
                // Store it to ram .
                if (msg_address >= 16'hA000 && msg_address <= 16'hA03F) begin
                    ram[msg_address - 16'hA000] <= msg_value;
                end
                // Reset the msg buffers.
                msg_address <= 0;
                msg_value <= 0;
            end
        endcase
        // Update the state counter.
        msg_state <= msg_state + 1;
    end else begin
        uart_transmit <= 1'b0;
    end
end

// Generate 100mhz clock signal
wire clk_100mhz;

pll_100mhz pll(
    .clock_in(clk_16mhz),
    .clock_out(clk_100mhz),
    .locked()
);

endmodule
