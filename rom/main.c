#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <gb/gb.h>
#include <gb/console.h>
#include "registers.h"

#define RAMC_REG (*(__REG)0x0000)
#define ENABLE_RAM RAMC_REG = 0x0A
#define DISABLE_RAM RAMC_REG = 0x00

uint8_t read_ram(uint16_t address)
{
    ENABLE_RAM;
    uint8_t value = (*(__REG)address);
    DISABLE_RAM;
    return value;
}

#define gate(chnl)                                           \
    bool nr ## chnl ## _rose = false;                        \
    bool nr ## chnl ## _gate = nr ## chnl ## 4 >> 7;         \
    if (nr ## chnl ## _gate && !prev_nr ## chnl ## _gate)    \
    {                                                        \
        nr ## chnl ## _gate_cnt++;                           \
        nr ## chnl ## _rose = true;                          \
    }                                                        \
    else                                                     \
    {                                                        \
        nr ## chnl ## 4 = nr ## chnl ## 4 & 0x7F;            \
    }                                                        \
    prev_nr ## chnl ## _gate = nr ## chnl ## _gate;

void main()
{
    DISPLAY_ON;
    NR52_REG = 0x80;
    NR51_REG = 0xFF;
    NR50_REG = 0x77;
    // TODO: gotoxy seems to fail until a printf happens.
    printf("  ");

    uint16_t nr1_gate_cnt = 0;
    uint16_t nr2_gate_cnt = 0;
    uint16_t nr3_gate_cnt = 0;
    uint16_t nr4_gate_cnt = 0;
    bool prev_nr1_gate = false;
    bool prev_nr2_gate = false;
    bool prev_nr3_gate = false;
    bool prev_nr4_gate = false;
    uint8_t counter = 0x00;
    while (1)
    {
        uint8_t nr10 = read_ram(NR10);
        uint8_t nr11 = read_ram(NR11);
        uint8_t nr12 = read_ram(NR12);
        uint8_t nr13 = read_ram(NR13);
        uint8_t nr14 = read_ram(NR14);
        gate(1);

        uint8_t nr21 = read_ram(NR21);
        uint8_t nr22 = read_ram(NR22);
        uint8_t nr23 = read_ram(NR23);
        uint8_t nr24 = read_ram(NR24);
        gate(2);

        uint8_t nr30 = read_ram(NR30);
        uint8_t nr31 = read_ram(NR31);
        uint8_t nr32 = read_ram(NR32);
        uint8_t nr33 = read_ram(NR33);
        uint8_t nr34 = read_ram(NR34);
        gate(3);

        uint8_t nr41 = read_ram(NR41);
        uint8_t nr42 = read_ram(NR42);
        uint8_t nr43 = read_ram(NR43);
        uint8_t nr44 = read_ram(NR44);
        gate(4);

        /////////////////////////

        if (nr1_rose)
        {
            NR10_REG = nr10;
            NR11_REG = nr11;
            NR12_REG = nr12;
            NR13_REG = nr13;
            NR14_REG = nr14;
        }

        if (nr2_rose)
        {
            NR21_REG = nr21;
            NR22_REG = nr22;
            NR23_REG = nr23;
            NR24_REG = nr24;
        }

        if (nr3_rose)
        {
            NR30_REG = nr30;
            NR31_REG = nr31;
            NR32_REG = nr32;
            NR33_REG = nr33;
            NR34_REG = nr34;
        }

        if (nr4_rose)
        {
            NR41_REG = nr41;
            NR42_REG = nr42;
            NR43_REG = nr43;
            NR44_REG = nr44;
        }

        /////////////////////////

        // TODO: Switch statement?
        if (joypad() && !(nr1_rose || nr2_rose || nr3_rose || nr4_rose))
        {
            if (counter % 5 == 0)
            {
                gotoxy(3, 4);
                printf("%02X %02X %02X %02X",
                        nr1_gate_cnt, nr2_gate_cnt, nr3_gate_cnt, nr4_gate_cnt);
            }
            else if (counter % 5 == 1)
            {
                gotoxy(3, 6);
                printf("%02X %02X %02X %02X %02X",
                        nr10, nr11, nr12, nr13, nr14);
            }
            else if (counter % 5 == 2)
            {
                gotoxy(3, 8);
                printf("%02X %02X %02X %02X",
                        nr21, nr22, nr23, nr24);
            }
            else if (counter % 5 == 3)
            {
                gotoxy(3, 10);
                printf("%02X %02X %02X %02X %02X",
                        nr30, nr31, nr32, nr33, nr34);
            }
            else if (counter % 5 == 4)
            {
                gotoxy(3, 12);
                printf("%02X %02X %02X %02X",
                        nr41, nr42, nr43, nr44);
            }
            counter++;
        }
    }
}
