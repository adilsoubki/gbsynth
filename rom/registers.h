#ifndef REGISTERS_H
#define REGISTERS_H

#define NR10 0xA000
#define NR11 0xA001
#define NR12 0xA002
#define NR13 0xA003
#define NR14 0xA004
#define NR21 0xA005
#define NR22 0xA006
#define NR23 0xA007
#define NR24 0xA008
#define NR30 0xA009
#define NR31 0xA00A
#define NR32 0xA00B
#define NR33 0xA00C
#define NR34 0xA00D
#define NR41 0xA00E
#define NR42 0xA00F
#define NR43 0xA010
#define NR44 0xA011

// TODO: Unused? Delete me.
const uint16_t registers[] = {
    NR10, NR11, NR12, NR13, NR14,
    NR21, NR22, NR23, NR24,
    NR30, NR31, NR32, NR33, NR34,
    NR41, NR42, NR43, NR44
};

#endif
